import { Flight } from "../component/my-from/flight";

export class Mockflight {

  public static mockflight: Flight[] = [
    {
      fullName: "Supakit Srisawai",
      from: "ไทย",
      to: "ญี่ปุ่น",
      type: "One way",
      adults: 1,
      departure: new Date('03-04-2565'),
      children: 1,
      infants: 1,
      arrival: new Date('03-05-2565')
    }
  ]
}
