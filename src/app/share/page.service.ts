import { Injectable } from '@angular/core';


import { Flight } from '../component/my-from/flight';
import { Mockflight } from './mockflight';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  PageFlight: Flight[] =[]

  constructor() {
    this.PageFlight = Mockflight.mockflight
  }

  getPages(){
    return this.PageFlight
  }

  addfilght(add:Flight){
    
    this.PageFlight.push(add)
  }
}
