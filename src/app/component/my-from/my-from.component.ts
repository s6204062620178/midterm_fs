import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Flight } from './flight';

import { PageService } from 'src/app/share/page.service';

@Component({
  selector: 'app-my-from',
  templateUrl: './my-from.component.html',
  styleUrls: ['./my-from.component.css']
})
export class MyFromComponent implements OnInit {

  flight !: Flight[]
  form !: FormGroup   // !: ให้มีค่าว่างไว้ก่อน
  startDate = new Date(Date.now())

  constructor(private fb:FormBuilder, public MockPage: PageService) {
    this.form = this.fb.group({
      fullName:['',Validators.required],
      from:[null,Validators.required],
      to:[null,Validators.required],
      type:['',Validators.required],
      adults:[0,Validators.required],
      departure:['',Validators.required],
      children:[0,Validators.required],
      infants:[0,Validators.required],
      arrival:['',Validators.required]

    })
  }

  ngOnInit(): void {
    this.getPage()
    console.log("test")

  }
  getPage(){

    this.flight = this.MockPage.getPages()

  }

  onSubmit(getfrom:Flight):void{
    console.log("test 2")
    const yearDeparture = getfrom.departure.getFullYear() + 543
    const yearArrival = getfrom.arrival.getFullYear() + 543
    getfrom.departure = new Date((getfrom.departure.getMonth() + 1) + '/' + getfrom.departure.getDate() + "/" + yearDeparture)
    getfrom.arrival = new Date((getfrom.arrival.getMonth() + 1) + '/' + getfrom.arrival.getDate() + "/" + yearArrival)

    this.MockPage.addfilght(getfrom)
  }

}
